class Waiter(val name:String, var order: List[String] = List()){
  println(s"My name $name")

  def giveMe(dish: String): Waiter = {
    this.order = dish::this.order
    this
  }

  def complete(): List[String] = {
    this.order.reverse.toList
  }
}

//
import scala.io.StdIn

object Main {
  def main(args: Array[String]) {
    val input:String = StdIn.readLine()
    val tokenDeleter = new StringProcessor {
      override def process(input: String):String = input.replaceAll("[!\\,\\.]", "")
    }


    val shortener =  new StringProcessor {
      override def process(input: String):String = {
        if(input.length > 20) input.slice(0,20) + "..."
        else input
      }}

    val toLowerConvertor = new StringProcessor {
      override def process(input: String):String = {
        input.toLowerCase()
      }
    }
    val result = shortener.process(tokenDeleter.process(toLowerConvertor.process(input)))
    println(result)
  }
}

//
class Point(val x:Double, val y:Double, val z:Double)

case object Point{

  def apply(x:Double, y:Double, z:Double):Point = {val temp:Point =  new Point(x,y,z); temp}

  def average(data:List[Point]): Point = {
    lazy val temp_x:Double = data.map(_.x).sum
    lazy val temp_y:Double = data.map(_.y).sum
    lazy val temp_z:Double = data.map(_.z).sum
    data match {
      case data if data.size > 1 => new Point(temp_x/data.size, temp_y/data.size, temp_z/data.size)
      case _ => new Point(0.0,0.0,0.0)
    }
  }

  def show(point:Point):String ={
    val xx = point.x
    val yy = point.y
    val zz = point.z
    s"$xx $yy $zz"
  }
}
//
class Cat extends Animal {
  val sound= "Meow!"
}

class Dog extends Animal {
  val sound= "Woof!"
}

class Fish extends Animal {
  val sound= "Fishes are silent!"
  override def voice:Unit = println(sound)
}

//
final case class Pair [T <:Any, S <: Any](first: T, second: S) {
  def swap = {Pair(second, first)}
}

val pair = Pair(123, "Oleg").swap
require(pair.first ==  "Oleg")
require(pair.second == 123)

//
case class Pair [T <% Ordered[T]](first: T, second: T) {
  def smaller = {
    if (first < second) first
    else second }
}

val p = Pair(BigInt("1000000000000000"),BigInt("7000000000000000"))
require(p.smaller == BigInt("1000000000000000"))
