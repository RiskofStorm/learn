var points: List[Int] = List(1,3)
var chr1: List[Char] = List('x', 'y', 'y', 'x', 'x')
var chr2: List[Char] = List('y', 'x', 'x', 'y', 'y')

def swap_genes(genes: (List[Char], List[Char]), n: Int): (List[Char], List[Char]) = {
  var list1_swap = genes._1.slice(n, genes._1.size)
  var list2_swap = genes._2.slice(n, genes._2.size)
  (genes._1.patch(n, list2_swap, list1_swap.size), genes._2.patch(n, list1_swap, list2_swap.size))
}
var tup:(List[Char], List[Char]) = (chr1, chr2)
for (p <- points) {
  var tup:(List[Char], List[Char]) = swap_genes(tup, p)
}
println(tup._1.mkString)
println(tup._2.mkString)
