import scala.util.matching.Regex
val cat_pattern = "(meow|nya)".r
val dog_pattern = "(Rex)".r
val robo_pattern = "([0|1])+".r


val kind = pet match {
  case pet if dog_pattern.pattern.matcher(pet.name).matches => "dog"
  case pet if cat_pattern.pattern.matcher(pet.says).matches => "cat"
  case pet if robo_pattern.pattern.matcher(pet.says).matches => "robot"
  case _ => "unknown"
}
println(kind)

//
val log: PartialFunction[Double, Double] = {
  case d:Double if d > 0 => Math.log(d)
}

//
def discount: PartialFunction[Jar,String] = {
  case Jar(name,value,price) if value >= 5 && value <= 10 => List(name, price * 0.05).mkString(" ")
  case Jar(name,value,price)  if value > 10 => List(name, price * 0.1).mkString(" ")
}

//
def swap3(tuple: (Int, Int, Int)): (Int, Int, Int) = {
  (tuple._3, tuple._2, tuple._1)
}
//
def foo(list: List[Int]): Int = {
  val test = list.find(_ % 3 == 0)
  if (!test.isEmpty) test.get * 2 else 0
}

//
def divide(p: (Int, Int))(q: (Int, Int)): Either[String, (Int, Int)] = {
  lazy val commDiv = BigInt(p._1 * q._2).gcd(BigInt(p._2 * q._1)).toInt

  val result = (p, q) match {
    case (p, q) if p._1 > p._2 || q._1 > q._2 => Left("Invalid input")
    case (p, q) if p._2 == 0 || q._2 == 0 || q._1 == 0 => Left("Zero divisor")
    case (p, q) if (p._1 * q._2) > (p._2 * q._1) => Left("Improper result")
    case _ => Right(((p._1 * q._2) / commDiv, (p._2 * q._1) / commDiv))
  }
  result
}

//
def flatten(options: List[Option[Int]]): List[Int] = {
  val filterNone: PartialFunction[Option[Int], Int] = {case i if i != None => i.get}
  options.collect(filterNone)
}

//
