
def fibs(num: Int): Int =  {
  if (num == 1) 1
  else if (num == 2) 1
  else fibs(num - 1) + fibs(num - 2)

}
//
def sendGift(currentAmount: Int, gift: => Int) = {
  if (currentAmount >= 500)
    currentAmount + gift
  else
    currentAmount
}
val accountAmounts = List(100, 200, 500, 300, 700)
//
val searchOdd = (LessonData.searchInArray _).curried(_ % 2 == 1)
//
import scala.annotation.tailrec
@tailrec
def fibs(n: Int, currentNumber: Int = 1, f1: BigInt = 0, f2: BigInt = 1): BigInt = {
  if (n == currentNumber) f2
  else fibs(n - 1, currentNumber, f2, (f1 + f2))
}
println(fibs(5))
//
import scala.collection.Iterable
import scala.math.ceil

def middle[T](data: Iterable[T]): Any= {
  if (data.size % 2 == 0) {
    val index = data.size / 2
    data.splitAt(index)._2.head
  }else{
    val index = ceil(data.size / 2).toInt
    data.splitAt(index)._2.head
  }
}
require(middle("Scala") == 'a')
require(middle(Seq(1, 7, 5, 9)) == 5)
//
