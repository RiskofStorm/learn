object Main {
  def main(args: Array[String]) {
    val ints: List[Int] = Lesson.ints
    val (left, right) = ints.sorted.splitAt(ints.count(_ == 0))
    println(left.mkString(","))
    println(right.mkString(","))
  }
}
//
import scala.io.StdIn

object Main {
  def main(args: Array[String]) {
    val k = StdIn.readInt()
    val list: List[Int] = StdIn.readLine().split(" ").map(_.toInt).toList

    def kOrder(list: List[Int], k: Int): Int = {
      val list2 = list.sorted
      list2(k - 1)
    }
    println(kOrder(list,k))
  }}

//
list.filter(_ < 100).filter(_ % 4 == 0).map(_ / 4).init.foreach(println)
//
import scala.io.StdIn

object Main {
  def main(args: Array[String]) {
    val data: Stream[String] = Stream.continually(Console.readLine).takeWhile(_ != "END")
    val filteredData:Seq[Int] = for(i <- 1 until data.size by 2) yield (data(i).toInt)
    println(filteredData.sum * 2)

  }
}

//
import scala.annotation.tailrec
object Main {
  def main(args: Array[String]) {
    val points: List[Int] = Lesson.points
    val chr1: List[Char] = Lesson.chr1
    val chr2: List[Char] = Lesson.chr2

    @tailrec def swap_genes(genes: (List[Char], List[Char]), points: List[Int]): (List[Char], List[Char]) = {
      points match {
        case Nil => genes
        case n::tail =>  swap_genes((genes._1.patch(n, genes._2.slice(n, genes._2.size), genes._2.size - n),
          genes._2.patch(n, genes._1.slice(n, genes._1.size), genes._1.size - n)), tail)
      }
    }
    var tup:(List[Char], List[Char]) = (chr1, chr2)
    tup = swap_genes(tup, points)

    println(tup._1.mkString(""))
    println(tup._2.mkString(""))
  }
}

//
import scala.annotation.tailrec
object Main {
  def main(args: Array[String]) {
    val points: List[Int] = Lesson.points
    val chr1: List[Char] = Lesson.chr1
    val chr2: List[Char] = Lesson.chr2

    @tailrec def swap_genes(genes: (List[Char], List[Char]), points: List[Int]): (List[Char], List[Char]) = {
      points match {
        case Nil => genes
        case n::tail =>  swap_genes((genes._1.patch(n, genes._2.slice(n, genes._2.size), genes._2.size - n),
          genes._2.patch(n, genes._1.slice(n, genes._1.size), genes._1.size - n)), tail)
      }


    }


    var tup:(List[Char], List[Char]) = (chr1, chr2)

    tup = swap_genes(tup, points)


    println(tup._1.mkString(""))
    println(tup._2.mkString(""))

  }
}

//
for {x <- list1
     y <- list2
     xy <- list3
     if x != y
     if x * y == xy} yield println(s"($x,$y)")

//
def result = for{
  serv1 <- service1
  serv2 <- service2(serv1)
  serv4 <- service4(serv1, serv2, service3)} yield serv4