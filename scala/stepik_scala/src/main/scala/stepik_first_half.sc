import scala.collection.mutable
import scala.math.BigDecimal.RoundingMode.HALF_UP

def crispsWeight(weight: BigDecimal, potatoWaterRatio: Double, crispsWaterRatio: Double): BigDecimal = {
  val before: BigDecimal = weight - (weight * BigDecimal(potatoWaterRatio))
  val after: BigDecimal = weight - (weight * BigDecimal(crispsWaterRatio))
  println(before, after)
  (weight / (after/before)).setScale(5, HALF_UP)
}

crispsWeight(90.0, 0.9,0.1)

import scala.io.StdIn

object Main {
  def main(args: Array[String]) {
    val digit = StdIn.readInt()
    var count: Int = 0
    for (i <- digit.toBinaryString ){
      count += i.toString.toInt}
    println(count)
  }
}



def isCapital(word: String, pos: Int): Boolean = {
  val sumbol = word(pos).toInt
  if (96 > sumbol && sumbol> 64) {return(true)}
  else (return false)
}

val nime = "ello there"
println(s"H${nime}")




//Sample Input:
//2 6
//foobarbaz
//Sample Output:
//fobraboaz

val temp = "2 6"
val word = "foobarbaz"
val index = temp.split(" ").map(i => i.toInt)
println(index)
val sub_word = word.substring(index(0), index(1)+1)
val new_word = word.replace(sub_word, sub_word.reverse)
println(new_word)


//Sample Input:
//snake_case
//
//Sample Output:
//
//true
import scala.util.matching.Regex
val pattern = "^([a-z]+_[a-z]+)+$".r
val pattern2 = "(?:^\\D)([a-z+\\_a-z]+)\\b(?<!\\_)".r
val snake = "snake_case"
val test = "_snake_case_"
println(pattern2.matches(snake))
println(pattern2.matches(test))

//def true_fibs(x:Int, y:Int):Int ={
//  val old_y: Int = y
//  val _y:Int = x + y
//  val _x:Int = old_y
//  print(x)
//  true_fibs(_x, _y)
//}
//true_fibs(1,1)

def fibs1(num: Int): Int =  {
  if (num == 1) 1
  else if (num == 2) 3
  else fibs1(num - 1) + fibs1(num - 2)

}




import scala.annotation.tailrec

def fibs(num: Int):Int = {

  @tailrec def wrapper(num: Int, prev: Int = 1, next: Int = 2): Int = {
    if (num == 0) prev
    else if (num == 1) next
    else wrapper(num - 1, next, (next + prev))
  }
  wrapper(num)
}

//fibs(3)


import scala.annotation.tailrec
@tailrec
def fibs(n: Int, currentNumber: Int = 1, f1: BigInt = 0, f2: BigInt = 1): BigInt = {
  if (n == currentNumber)
    f2
  else {
    fibs(n - 1, currentNumber, f2, (f1 + f2))
  }
}
//println(fibs(n))



import scala.collection.Iterable
import scala.math.ceil

def middle[T](data: Iterable[T]): Any= {
  if (data.size % 2 == 0) {
    val index = data.size / 2
    data.splitAt(index)._2.head
  }else{
    val index = ceil(data.size / 2).toInt
    data.splitAt(index)._2.head
  }
}
middle("Scala") == 'a'
middle(Seq(1, 7, 5, 9)) == 5
require(middle("Scala") == 'a')
require(middle(Seq(1, 7, 5, 9)) == 5)