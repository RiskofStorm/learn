

final case class Pair [T <:Any, S <: Any](first: T, second: S) {
  def swap = {new Pair(second, first)}
}

val pair = Pair(123, "Oleg").swap
require(pair.first ==  "Oleg")
require(pair.second == 123)