case class Pet(name: String, says: String)




//val pet = Pet("rumble", "meow")
val pet = Pet("Rex", "wof")

//val pet = Pet("dumb", "0101010011")

val cat_pattern = "(meow|nya)".r
val dog_pattern = "(Rex)".r
val robo_pattern = "([0|1])+".r


val kind = pet match {
  case pet if dog_pattern.pattern.matcher(pet.name).matches => "dog"
  case pet if cat_pattern.pattern.matcher(pet.says).matches => "cat"
  case pet if robo_pattern.pattern.matcher(pet.says).matches => "robot"
  case _ => "unknown"
}
println(kind)


//val email = "(\\w+@\\w+\\.\\w+)".r
//val name = "^[a-zA-Z]*?(?=\\ |\\n)".r

val input = List("oleg oleg@email.com", "7bdaf0a1be3", "a8af118b1a2", "28d74b7a3fe")
val input2 = List("Oleg ol@gmail.com", "asdasdasdasd")

val input3  = List("Oleg 123@gmail.com", "123@gmaiasdl.com")
//>>> Oleg gmail.com
val input4  = List("Oleg ol@gmail.com", "asdasdasdasd")
//>>> Oleg gmail.com
val input5 = List("Oleg oleg@gmail.com", "7bdaf0a1be3", "a8af118b1a2", "28d74b7a3fe")
// Oleg gmail.com


val full = "^[a-zA-Z]+(?=\\ |\\n|$)|(?!\\w+\\@)\\w+\\.\\w+".r

val name = "^[a-zA-Z]+(?=\\ |\\n|$)".r
val email = "(?!\\w+\\@)\\w+\\.\\w+".r



val full = "^[a-zA-Z]+(?=\\ |\\n|$)|(?!\\w+\\@)\\w+\\.\\w+".r
val name = "^[a-zA-Z]+(?=\\ |\\n|$)".r
val email = "(?!\\w+\\@)\\w+\\.\\w+".r

val result = input match {
  case a::b::xs if full.findAllMatchIn(a).toArray.slice(1,2) > 2 => full.findAllMatchIn(a).mkString(" ")
  case a::b::xs if full.findAllMatchIn(a).size == 1 =>  name.findAllMatchIn(a).mkString + " " + email.findAllMatchIn(b).mkString
  case Nil  => "invalid"
  case _ => "invalid"


}

println(result)




println(input.mkString("\n"))

println(full.findAllIn(test).mkString)


val full = "^[a-zA-Z]+(?=\\ |\\n|$)|(?!\\w+\\@)\\w+\\.\\w+".r


val result = input match {

  case first :: second :: tail if full.findAllIn(first).size >= 2
  => full.findAllIn(first).mkString(" ")
  case first :: second :: tail if full.findAllIn(second).size >= 1
  => name.findAllIn(first).mkString + " " +  email.findAllIn(second).mkString
  case _ => "invalid"

}

println(result)




val log: PartialFunction[Double, Double] = {
  case d:Double if d > 0 => Math.log(d)
}


def discount: PartialFunction[Jar,String] = {
  case Jar(name,value,price) if value > 5 && value < 10 => List(name,value, price * 0.05).mkString
  case Jar(name,value,price)  if value > 10 => List(name, value, price * 0.1).mkString
}


