val points: List[Int] = List(1, 3)
val chr1: List[Char] = List('x', 'x', 'x', 'x', 'x')
val chr2: List[Char] = List('y', 'y', 'y', 'y', 'y')

import scala.annotation.tailrec

@tailrec def swap_genes(genes: (List[Char], List[Char]), points: List[Int]): (List[Char], List[Char]) = {
  points match {
    case Nil => genes
    case n :: tail => swap_genes((genes._1.patch(n, genes._2.slice(n, genes._2.size), genes._2.size - n),
      genes._2.patch(n, genes._1.slice(n, genes._1.size), genes._1.size - n)), tail)
  }


}


var tup: (List[Char], List[Char]) = (chr1, chr2)

tup = swap_genes(tup, points)


println(tup._1.mkString(""))
println(tup._2.mkString(""))



//val sad =  for { x <- Some(2); y <- List(1,2,3,4) } yield x + y
val list1 = List(1, 3, 5, 7)
val list2 = List(2, 4, 6, 8)
val list3 = List(1, 3, 5, 8, 10, 12, 14)


for {x <- list1.sorted
     y <- list2.sorted
     xy <- list3
     if x != y
     if x * y == xy


     } println(s"($x, $y)")

def service1: Either[String, Double]
def service2(res1: Double): Either[String, Int]
def service3: String
def service4(res1: Double, res2: Int, res3: String): Either[String, String]

def result = for{ s1 <- service1
                  s2 <- service2(s1)
                  s4 <- service4(s1, s2, service3)} yield s4


// Main реализовывать не нужно
// напишите вашу реализацию класса Waiter
class Waiter(val name: String, var order: List[String] = List[String]()) {
  println(s"My name $name")


  def giveMe(dish: String): Waiter = {
    this.order = dish :: this.order
    this
  }

  def complete(): List[String] = {
    this.order.reverse.toList
  }

}

val sss = new Waiter("vasii")
val rest = sss.giveMe("borsh").giveMe("bread").complete()
println(rest.mkString)

import scala.io.StdIn

trait StringProcessor {
  def process(input: String): String
}


import scala.io.StdIn

object Main {
  def main(args: Array[String]) {
    val input: String = StdIn.readLine()
    val tokenDeleter = new StringProcessor {
      override def process(input: String): String = input.replaceAll("[,!?]", "")
    }


    val shortener = new StringProcessor {
      override def process(input: String): String = {
        if (input.length > 20) input.slice(0, 20) + " ..."
        else input
      }
    }

    val toLowerConvertor = new StringProcessor {
      override def process(input: String): String = {
        input.toLowerCase()
      }
    }
    val result = shortener.process(toLowerConvertor.process(input))
    println(result)
  }
}


class Point(val x: Double, val y: Double, val z: Double) {
  override def toString() = (x, y, z).toString()
}

case object Point {
  def apply(x: Double, y: Double, z: Double): Point = {
    val temp: Point = new Point(x, y, z); temp
  }

  def average(data: List[Point]): List[Double] = {
    lazy val temp_x: Double = data.map(_.x).sum
    lazy val temp_y: Double = data.map(_.y).sum
    lazy val temp_z: Double = data.map(_.z).sum
    data match {
      case data if data.size > 1 => List(temp_x / data.size, temp_y / data.size, temp_z / data.size)
      case _ => List(data.head.x, data.head.y, data.head.z)
    }
  }

  def show(point: Point): String = {
    s"$point.x $point.y $point.z"
  }
}


val fir = Point(1, 2, 3)
val sec = Point(2, 3, 4)
val ter: List[Point] = List(fir, sec)
Point.average(ter)



//import scala.io.StdIn
//
//object FacedString {
//  def apply(input: String) = s"*_*$input*_*"
//
//  def unapply(arg: String): Option[String] = {
//    val stringArray: Array[String] = arg.split("*_*")
//    if (stringArray.tail.nonEmpty) Some(stringArray.head) else None
//  }
//}
//
//object Main {
//    def main(args: Array[String]) {
//      val name:String = StdIn.readLine
//      val fs = FacedString(name)
//      fs match{
//        case FacedString(name) => println(name)
//        case _ => println("Could not recognize string")
//      }
//    }
//  }



def divide(p: (Int, Int))(q: (Int, Int)): Either[String, (Int, Int)] = {
  lazy val commDiv = BigInt(p._1 * q._2).gcd(BigInt(p._2 * q._1)).toInt

  val result = (p, q) match {
    case (p, q) if p._1 > p._2 || q._1 > q._2 => Left("Invalid input")
    case (p, q) if p._2 == 0 || q._2 == 0 || q._1 == 0 => Left("Zero divisor")
    case (p, q) if (p._1 * q._2) > (p._2 * q._1) => Left("Improper result")
    case _ => Right(((p._1 * q._2) / commDiv, (p._2 * q._1) / commDiv))
  }
  result
}


def flatten(options: List[Option[Int]]): List[Int] = {
  val filterNone: PartialFunction[Option[Int], Int] = {
    case i if i != None => i.get
  }
  options.collect(filterNone)
}




def result = service4(
  for {serv1 <- if service1.isRight service1.right.get else Left(service1.left.get)
       serv2 <- if service2(serv1).isRight service2.right.get else Left(service2.left.get)
       serv3 <- Right(service3)} yield serv1 + serv2 + serv3)




