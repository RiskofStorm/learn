import sys

data = dict()

for read in sys.stdin:
    temp = read.split("\t")
    if temp[0] in data.keys():
        data[temp[0]] += 1
    else:
        data[temp[0]] = 1
for k, v in data.items():
    k = k.split("#")
    print(k[0] + "\t" + k[1] + "\t" + str(v))