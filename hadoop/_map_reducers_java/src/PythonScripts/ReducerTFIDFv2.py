import sys
import re

word_find = re.compile("[A-Za-z]+")
wordCount = dict()
wordData = list()
data = []
for read in sys.stdin:
    word = read.split("\t")[0]
    count = read.split("\t")[1]
    count = count.split(";")

    if word not in wordCount:
        wordCount[word] = 1
    else:
        wordCount[word] += 1

    fullword = word + "#" + count[0]
    wordData.append((fullword, count[1]))

for k,v in wordCount.items():
    for i in wordData:
        keypass = word_find.findall(i[0])[0]
        if k == keypass:
            print(i[0] + "\t" + i[1] + "\t" + str(v))
