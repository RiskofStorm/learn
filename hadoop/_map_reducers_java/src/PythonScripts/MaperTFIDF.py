import sys
import re

rWords = re.compile(u'[A-Za-z]+', re.UNICODE)
rDoc = re.compile('(\d+):')

for read in sys.stdin:
    tempDoc = rDoc.findall(read)
    tempDoc = tempDoc[0].replace(":", "")
    subtemp = rDoc.sub("",read)
    subtemp = rWords.findall(subtemp)
    for i in subtemp:
        print(i + "#" + tempDoc + "\t" + "1")
