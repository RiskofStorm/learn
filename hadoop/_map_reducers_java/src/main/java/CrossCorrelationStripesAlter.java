

import java.util.HashMap;

public class CrossCorrelationStripesAlter {
    public static void mapper(String[] args) throws java.io.IOException {
        String text;
        String[] parsedData;

        java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));

        while (!(text = br.readLine()).equals("")) {
            parsedData = text.split(" ");

            for (String i : parsedData) {
                HashMap<String, Integer> wordCount = new HashMap<>();
                StringBuilder output = new StringBuilder();

                for (String j : parsedData) {
                    if (!i.equals(j)) {
                        if (wordCount.containsKey(j))
                            wordCount.put(j, wordCount.get(j) + 1);
                    } else {
                        wordCount.put(j, 1);
                    }

                }
//                wordCount.forEach((k, v) -> output.append(k + ":" + v));
                wordCount.forEach((k,v)-> System.out.println(k +"\t"+ v));
//                System.out.println(i + "\t" + wordCount);
            }
        }
    }
}
