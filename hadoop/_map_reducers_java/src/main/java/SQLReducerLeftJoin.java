import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class SQLReducerLeftJoin {
    public static void main(String[] args) throws Exception {

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String temp;
        String[] parsedData;
        HashMap<String, ArrayList<Integer>> dict = new HashMap<>();
        while ((temp = input.readLine()) != null) {
            parsedData = temp.split("\t");
            if (dict.get(parsedData[1]) == null) {
                ArrayList<Integer> tempArray = new ArrayList<>();
                tempArray.add(Integer.parseInt(parsedData[0]));
                dict.put(parsedData[1], tempArray);
            } else {
                ArrayList<Integer> array = dict.get(parsedData[1]);
                array.add(Integer.parseInt(parsedData[0]));
                dict.put(parsedData[1], array);
            }
            (dict.get(dict.keySet().toArray()[0])).removeAll(dict.get(dict.keySet().toArray()[1]));
            (dict.get(dict.keySet().toArray()[0])).forEach(i -> System.out.println(i));
            dict.clear();
        }
    }
}
