import org.apache.commons.lang.StringUtils;

import java.util.HashMap;

public class CrossCorrelationStripes {
    public static void mapper(String[] args) throws java.io.IOException {
        String text;
        String[] parsedData;

        java.io.BufferedReader br = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
        HashMap<String, String> wordCount = new HashMap<>();
        while ((text = br.readLine()) != null) {
            parsedData = text.split("\t");

            for (String i : parsedData) {
                StringBuilder keyValue = new StringBuilder();
                for (String j : parsedData) {
                    if (!i.equals(j)) {
                        if (!keyValue.toString().equals("")) {
                            keyValue.append(",");
                        } else {
                            keyValue.append(j);
                            keyValue.append(":");
                            keyValue.append(StringUtils.countMatches(parsedData.toString(), j));
                            wordCount.put(i, keyValue.toString());

                        }
                    }

                }
                wordCount.forEach((k, v) -> System.out.println(k + "\t" + v));

            }
        }
    }
}
