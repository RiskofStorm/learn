import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;

public class FoolReducer {
    public static void foolReducer(String[] args) throws IOException {
        String text;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        LinkedHashMap<String, Integer> wordCount = new LinkedHashMap<>();

        while ((text = br.readLine()) != null) {
            String[] words = text.split("\t");

            if (wordCount.get(words[0]) != null) {
                wordCount.put(words[0], wordCount.get(words[0]) + Integer.parseInt(words[1]));
            } else {
                wordCount.forEach((k, v) -> System.out.println(k + "\t" + v));
                wordCount.clear();
                wordCount.put(words[0], Integer.parseInt(words[1]));
            }

        }
        wordCount.forEach((k, v) -> System.out.println(k + "\t" + v));
    }
}

