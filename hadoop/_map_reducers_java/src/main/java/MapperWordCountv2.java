import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class MapperWordCountv2 {
    public static void countWordv2(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, Integer> wordCount = new HashMap<>();
        String text;
        while ((text = br.readLine()) != null) {
            String[] textList = text.split(" ");
            for (String word : textList) {
                if (wordCount.get(word) != null) {
                    wordCount.put(word, wordCount.get(word) + Integer.parseInt("1"));
                } else {

                    wordCount.put(word, Integer.parseInt("1"));
                }
            }
        }
        wordCount.forEach((k, v) -> System.out.println(k + "\t" + v));

    }
}
