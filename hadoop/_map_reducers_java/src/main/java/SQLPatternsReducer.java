import java.util.ArrayList;
import java.util.HashSet;
import java.util.*;
import org.apache.commons.collections.*;

public class SQLPatternsReducer {
    public static void main(String[] args) throws Exception {
        String read;
        String[] parseRead;
        java.io.BufferedReader buf = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
        java.util.ArrayList<Integer> temp = new java.util.ArrayList<>();
        java.util.HashMap<String, java.util.ArrayList<Integer>> dict = new java.util.HashMap<>();
        String a = "A";
        String b = "B";

        while ((read = buf.readLine()) != null) {

            parseRead = read.split("\t");
            if (dict.containsKey(parseRead[1])) {
                java.util.ArrayList<Integer> tempDict = dict.get(parseRead[1]);
                tempDict.add(Integer.parseInt(parseRead[0]));
                dict.put(parseRead[1], tempDict);

            } else {
                java.util.ArrayList<Integer> tempValue = new java.util.ArrayList<>();
                tempValue.add(new Integer(parseRead[0]));
                dict.put(parseRead[1], tempValue);

            }
            try {
                java.util.HashSet<Integer> setA = new java.util.HashSet<>(dict.get(a));
                java.util.HashSet<Integer> setB = new java.util.HashSet<>(dict.get(b));
                setA.disjunction(setB); // fix
                dict.clear();
                setA.forEach(i -> System.out.println(i));
            } catch (NullPointerException npe) {

            }

        }


    }
}