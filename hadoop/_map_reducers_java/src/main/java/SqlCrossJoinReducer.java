
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class SqlCrossJoinReducer {
    public static void main(String[] args) throws Exception {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String read;
        String[] readParsed;
        HashMap<String, ArrayList<String>> data = new HashMap<>();

        while ((read = input.readLine()) != null) {
            readParsed = read.split("\t");
            if (!data.containsKey(readParsed[0])) {
                ArrayList<String> subData = new ArrayList<>();
                subData.add(readParsed[1]);
                subData.add(readParsed[2]);
                data.put(readParsed[0], subData);

            }else{
                ArrayList<String> subData = data.get(readParsed[0]);
                subData.add(readParsed[1]);
                subData.add(readParsed[2]);
                data.put(readParsed[0], subData);

            }

        }
    }
}
